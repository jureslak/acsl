# Jure Slak
# Python, version 3.3

from itertools import combinations as comb

a, b = map(list, input().split())

for t in range(5):
    n, m, d = map(int, input().split()) # lambde in list comperhensioni so vasi prijatelji
    p = max((sorted(c,reverse=True) for c in comb(a,n) if sum(map(int,c)) % 5 == 0), key=lambda x: sum(map(int,x)))
    q = max((sorted(c,reverse=True) for c in comb(b,m) if sum(map(int,c)) % 5 == 0), key=lambda x: sum(map(int,x)))
    i, j = p.index(str(d)), q.index(str(d)) # avtomatsko prva pojavitev

    print('\n'.join(' '.join(p) if k == j else ' '*2*i+q[k] for k in range(m))+'\n---------------') # za manj vrstic :P
