class Tree:
	def __init__(self):
		self.value = None
		self.left = None
		self.right = None

	def insert(self, v):
		if self.value is None:
			self.value = v
			self.left = Tree()
			self.right= Tree()

		elif v <= self.value:
			self.left.insert(v)

		elif v > self.value:
			self.right.insert(v)

	def print(self, z=0):
		if self.value is not None:
			print(z*' ', self.value)
			self.left.print(z+2)
			self.right.print(z+2)

	def nodes(self):
		if self.value is None:
			return 0
		else:
			return self.left.nodes()+self.right.nodes()+1

	def ipl(self):
		if self.value is None:
			return 0
		else:
			return self.left.ipl()+self.left.nodes()+self.right.ipl()+self.right.nodes()

if __name__ == '__main__':
	a = Tree()
	a.insert('h')
	a.insert('l')
	a.insert('s')
	a.insert('A')
	a.insert('h')
	a.insert('H')
	a.print()
	print(a.nodes())
	print(a.ipl())