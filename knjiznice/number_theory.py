"""Module containing useful functions from the field of number theory,
ie. dealing with prime numbers, divisors, etc."""

def divisors(n):
    """Returns sorted list of divisors for a given positive integer n.
    It runs in O(sqrt(n)).
    >>> divisors(18)
    [1, 2, 3, 6, 9, 18]
    >>> divisors(64)
    [1, 2, 4, 8, 16, 32, 64]
    """

    if not isinstance(n, int):
        raise ValueError("n must be an integer, got type '{}' with value '{}'".format(type(n), n))
    if n <= 0:
        raise ValueError("n must be positive, got '{}'".format(n))
    result = []
    i = 1
    while i*i < n:
        if n % i == 0:
            result.extend([i, n//i])
        i += 1
    if i*i == n: result.append(i)  # ce je popoln kvadrat
    result.sort()
    return result

def primes(n):
    """Returns sorted list of primes lower than n.
    Sieve of Eratosthenes.
    >>> primes(18)
    [2, 3, 5, 7, 11, 13, 17]
    >>> primes(29)
    [2, 3, 5, 7, 11, 13, 17, 19, 23]
    """
    if not isinstance(n, int):
        raise ValueError("n must be an integer, got type '{}' with value '{}'".format(type(n), n))
    if n <= 0:
        raise ValueError("n must be positive, got '{}'".format(n))
    if n == 1:
        return list()

    p = list(range(n))
    p[1] = 0
    p[4 : n : 2] = [0]*len(range(4, n, 2))
    for i in range(3, int(n**0.5)+1, 2):
        if p[i]:
            p[i*i : n : i] = [0]*len(range(i*i, n, i))
    return [i for i in p if i]

def is_prime(p):
    """Returns True if given integer p is a prime.
    It runs in O(sqrt(p)).
    >>> is_prime(31)
    True
    >>> is_prime(49)
    False
    """
    if not isinstance(p, int):
        raise ValueError("p must be an integer, got type '{}' with value '{}'".format(type(p), p))
    if p <= 1: return False
    i = 2
    while i*i <= p:
        if p % i == 0:
            return False
        i += 1
    return True

def factorize(n):
    """Returns sorted list of factors of n.
    It runs in O(sqrt(n))
    >>> factorize(14)
    [2, 7]
    >>> factorize(24)
    [2, 2, 2, 3]
    """
    if not isinstance(n, int):
        raise ValueError("n must be an integer, got type '{}' with value '{}'".format(type(n), n))
    if n <= 0:
        raise ValueError("n must be positive, got '{}'".format(n))
    factors = []
    if n == 1:
        return factors
    i = 2
    while n > 1 and i*i <= n:
        while n % i == 0:
            factors.append(i)
            n //= i
        i += 1
    if n != 1:
        factors.append(n)
    return factors


if __name__ == '__main__':
    import doctest
    doctest.testmod()
