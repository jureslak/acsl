r,t = map(int, raw_input().split())
rez = 0
while t >= 0:
    t -= (r+1)**2 - r**2
    r += 2
    rez += 1
print rez-1
