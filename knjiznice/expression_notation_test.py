import unittest
from expression_notation import *

class TestExpressionNotation(unittest.TestCase):

    def test_evaluate_infix(self):
        # one operation
        self.assertEqual(evaluate_infix('1 + 1'), 2)
        self.assertEqual(evaluate_infix('5 * 7'), 35)
        self.assertEqual(evaluate_infix('5 / 2'), 2.5)
        self.assertEqual(evaluate_infix('5 // 2'), 2)
        self.assertEqual(evaluate_infix('6 % 4'), 2)
        self.assertEqual(evaluate_infix('2 ** 3'), 8)
        self.assertEqual(evaluate_infix('1000 + 1'), 1001)
        # multiple operations
        self.assertEqual(evaluate_infix('1 + 4 ** 2 + 3 ** 2 - 5 * 5 - 1'), 0)
        self.assertEqual(evaluate_infix('(1 + 5) / (2 * 5)'), 0.6)
        self.assertEqual(evaluate_infix('1 +- 1'), 0)
        self.assertEqual(evaluate_infix('1 -------- 1'), 2)
        # no operations
        self.assertEqual(evaluate_infix('0'), 0)
        self.assertEqual(evaluate_infix('42'), 42)
        # exceptions
        self.assertRaisesRegex(ValueError,
            "s must be a string, got type '<class 'int'>' with value '123'", evaluate_infix, 123)

    def test_evaluate_postfix(self):
        # one operation
        self.assertEqual(evaluate_postfix('1 1 +'), 2)
        self.assertEqual(evaluate_postfix('5 7 *'), 35)
        self.assertEqual(evaluate_postfix('5 2 /'), 2.5)
        self.assertEqual(evaluate_postfix('5 2 //'), 2)
        self.assertEqual(evaluate_postfix('6 4 %'), 2)
        self.assertEqual(evaluate_postfix('2 3 **'), 8)
        self.assertEqual(evaluate_postfix('1000 1 +'), 1001)
        self.assertEqual(evaluate_postfix('12 12 -'), 0)
        # multiple operations
        self.assertEqual(evaluate_postfix('1 4 2 ** + 3 2 ** 5 5 * 1 - - +'), 2)
        self.assertEqual(evaluate_postfix('1 5 + 2 5 * /'), 0.6)
        self.assertEqual(evaluate_postfix('3 2 * 5 5 + - 7 +'), 3)
        self.assertEqual(evaluate_postfix('8 4 // 2 ** 3 4 - +'), 3)
        self.assertEqual(evaluate_postfix('999 111 / 10 +'), 19)
        # no operations
        self.assertEqual(evaluate_postfix('0'), 0)
        self.assertEqual(evaluate_postfix('42'), 42)
        # exceptions
        self.assertRaisesRegex(ValueError,
            "s must be a string, got type '<class 'int'>' with value '123'", evaluate_postfix, 123)
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '5 \+ 5'", evaluate_postfix, '5 + 5')
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '5 3 4 -'", evaluate_postfix, '5 3 4 -')
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '3 \*'", evaluate_postfix, '3 *')

    def test_evaluate_prefix(self):
        # one operation
        self.assertEqual(evaluate_prefix('+ 1 1'), 2)
        self.assertEqual(evaluate_prefix('* 5 7'), 35)
        self.assertEqual(evaluate_prefix('/ 5 2'), 2.5)
        self.assertEqual(evaluate_prefix('// 5 2'), 2)
        self.assertEqual(evaluate_prefix('% 6 4'), 2)
        self.assertEqual(evaluate_prefix('** 2 3'), 8)
        self.assertEqual(evaluate_prefix('+ 1000 1'), 1001)
        self.assertEqual(evaluate_prefix('- 12 12'), 0)
        # multiple operations
        self.assertEqual(evaluate_prefix('+ + 1 ** 4 2 - ** 3 2 - * 5 5 1'), 2)
        self.assertEqual(evaluate_prefix('/ + 1 5 * 2 5'), 0.6)
        self.assertEqual(evaluate_prefix('+ * 3 2 - + 5 5 7'), 9)
        self.assertEqual(evaluate_prefix('+ ** // 8 4 2 - 3 4'), 3)
        self.assertEqual(evaluate_prefix('+ / 999 111 10'), 19)
        # no operations
        self.assertEqual(evaluate_prefix('0'), 0)
        self.assertEqual(evaluate_prefix('42'), 42)
        # exceptions
        self.assertRaisesRegex(ValueError,
            "s must be a string, got type '<class 'int'>' with value '123'", evaluate_prefix, 123)
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '5 \+ 5'", evaluate_prefix, '5 + 5')
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '- 5 3 4'", evaluate_prefix, '- 5 3 4')
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '\* 3'", evaluate_prefix, '* 3')

    def test_convert_postfix_to_prefix(self):
        # one operation
        self.assertEqual(convert_postfix_to_prefix('1 1 +'), '+ 1 1')
        self.assertEqual(convert_postfix_to_prefix('A B *'), '* A B')
        self.assertEqual(convert_postfix_to_prefix('var1 var2 -'), '- var1 var2')
        self.assertEqual(convert_postfix_to_prefix('3 6 //'), '// 3 6')
        # multiple operations
        self.assertEqual(convert_postfix_to_prefix('A B C + +'), '+ A + B C')
        self.assertEqual(convert_postfix_to_prefix('A B + C +'), '+ + A B C')
        self.assertEqual(convert_postfix_to_prefix('2 3 + 5 7 * 4 5 - + -'), '- + 2 3 + * 5 7 - 4 5')
        self.assertEqual(convert_postfix_to_prefix('3 2 * 5 5 + 7 - +'), '+ * 3 2 - + 5 5 7')
        self.assertEqual(convert_postfix_to_prefix('A B C * +'), '+ A * B C')
        self.assertEqual(convert_postfix_to_prefix('A B + C + D +'), '+ + + A B C D')
        self.assertEqual(convert_postfix_to_prefix('A B C * + D +'), '+ + A * B C D')
        # no operations
        self.assertEqual(convert_postfix_to_prefix('0'), '0')
        self.assertEqual(convert_postfix_to_prefix('42'), '42')
        # exceptions
        self.assertRaisesRegex(ValueError,
            "s must be a string, got type '<class 'int'>' with value '123'", convert_postfix_to_prefix, 123)
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '5 \+ 5'", convert_postfix_to_prefix, '5 + 5')
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '5 3 4 -'", convert_postfix_to_prefix, '5 3 4 -')
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '\* 3'", convert_postfix_to_prefix, '* 3')

    def test_convert_postfix_to_infix(self):
        # one operation
        self.assertEqual(convert_postfix_to_infix('1 1 +'), '(1 + 1)')
        self.assertEqual(convert_postfix_to_infix('A B *'), '(A * B)')
        self.assertEqual(convert_postfix_to_infix('var1 var2 -'), '(var1 - var2)')
        self.assertEqual(convert_postfix_to_infix('3 6 //'), '(3 // 6)')
        # multiple operations
        self.assertEqual(convert_postfix_to_infix('A B C + +'), '(A + (B + C))')
        self.assertEqual(convert_postfix_to_infix('A B + C +'), '((A + B) + C)')
        self.assertEqual(convert_postfix_to_infix('2 3 + 5 7 * 4 5 - + -'), '((2 + 3) - ((5 * 7) + (4 - 5)))')
        self.assertEqual(convert_postfix_to_infix('3 2 * 5 5 + 7 - +'), '((3 * 2) + ((5 + 5) - 7))')
        self.assertEqual(convert_postfix_to_infix('A B C * +'), '(A + (B * C))')
        self.assertEqual(convert_postfix_to_infix('A B + C + D +'), '(((A + B) + C) + D)')
        self.assertEqual(convert_postfix_to_infix('A B C * + D +'), '((A + (B * C)) + D)')
        # no operations
        self.assertEqual(convert_postfix_to_infix('0'), '0')
        self.assertEqual(convert_postfix_to_infix('42'), '42')
        # exceptions
        self.assertRaisesRegex(ValueError,
            "s must be a string, got type '<class 'int'>' with value '123'", convert_postfix_to_infix, 123)
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '5 \+ 5'", convert_postfix_to_infix, '5 + 5')
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '5 3 4 -'", convert_postfix_to_infix, '5 3 4 -')
        self.assertRaisesRegex(ValueError,
            "invalid postfix expression: '\* 3'", convert_postfix_to_infix, '* 3')

    def test_convert_prefix_to_postfix(self):
        # one operation
        self.assertEqual(convert_prefix_to_postfix('+ 1 1'), '1 1 +')
        self.assertEqual(convert_prefix_to_postfix('* A B'), 'A B *')
        self.assertEqual(convert_prefix_to_postfix('- var1 var2'), 'var1 var2 -')
        self.assertEqual(convert_prefix_to_postfix('// 3 6'), '3 6 //')
        # multiple operations
        self.assertEqual(convert_prefix_to_postfix('+ + A B C'), 'A B + C +')
        self.assertEqual(convert_prefix_to_postfix('+ A + B C'), 'A B C + +')
        self.assertEqual(convert_prefix_to_postfix('- + 2 3 + * 5 7 - 4 5'), '2 3 + 5 7 * 4 5 - + -')
        self.assertEqual(convert_prefix_to_postfix('+ * 3 2 - + 5 5 7'), '3 2 * 5 5 + 7 - +')
        self.assertEqual(convert_prefix_to_postfix('+ A * B C'), 'A B C * +')
        self.assertEqual(convert_prefix_to_postfix('+ + + A B C D'), 'A B + C + D +')
        self.assertEqual(convert_prefix_to_postfix('+ + A * B C D'), 'A B C * + D +')
        # no operations
        self.assertEqual(convert_prefix_to_postfix('0'), '0')
        self.assertEqual(convert_prefix_to_postfix('42'), '42')
        # exceptions
        self.assertRaisesRegex(ValueError,
            "s must be a string, got type '<class 'int'>' with value '123'", convert_prefix_to_postfix, 123)
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '5 \+ 5'", convert_prefix_to_postfix, '5 + 5')
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '- 5 3 4'", convert_prefix_to_postfix, '- 5 3 4')
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '\* 3'", convert_prefix_to_postfix, '* 3')

    def test_convert_prefix_to_infix(self):
        # one operation
        self.assertEqual(convert_prefix_to_infix('+ 1 1'), '(1 + 1)')
        self.assertEqual(convert_prefix_to_infix('* A B'), '(A * B)')
        self.assertEqual(convert_prefix_to_infix('- var1 var2'), '(var1 - var2)')
        self.assertEqual(convert_prefix_to_infix('// 3 6'), '(3 // 6)')
        # multiple operations
        self.assertEqual(convert_prefix_to_infix('+ + A B C'), '((A + B) + C)')
        self.assertEqual(convert_prefix_to_infix('+ A + B C'), '(A + (B + C))')
        self.assertEqual(convert_prefix_to_infix('- + 2 3 + * 5 7 - 4 5'), '((2 + 3) - ((5 * 7) + (4 - 5)))')
        self.assertEqual(convert_prefix_to_infix('+ * 3 2 - + 5 5 7'), '((3 * 2) + ((5 + 5) - 7))')
        self.assertEqual(convert_prefix_to_infix('+ A * B C'), '(A + (B * C))')
        self.assertEqual(convert_prefix_to_infix('+ + + A B C D'),  '(((A + B) + C) + D)')
        self.assertEqual(convert_prefix_to_infix('+ + A * B C D'), '((A + (B * C)) + D)')
        # no operations
        self.assertEqual(convert_prefix_to_infix('0'), '0')
        self.assertEqual(convert_prefix_to_infix('42'), '42')
        # exceptions
        self.assertRaisesRegex(ValueError,
            "s must be a string, got type '<class 'int'>' with value '123'", convert_prefix_to_infix, 123)
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '5 \+ 5'", convert_prefix_to_infix, '5 + 5')
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '- 5 3 4'", convert_prefix_to_infix, '- 5 3 4')
        self.assertRaisesRegex(ValueError,
            "invalid prefix expression: '\* 3'", convert_prefix_to_infix, '* 3')

if __name__ == '__main__':
    unittest.main()
