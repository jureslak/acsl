def value(s):
    s = s.replace('^','**')
    if style == 'infix': return eval(s)
    if style == 'postfix':
        v = []
        for t in s.split():
            try: v.append(float(t))
            except ValueError:
                a, b = v.pop(), v.pop()
                v.append(eval(str(b)+' '+t+' '+str(a)))
        return float(v.pop())
    if style == 'prefix':
        v = []
        for t in reversed(s.split()):
            try: v.append(float(t))
            except ValueError:
                a, b = v.pop(), v.pop()
                v.append(eval(str(a)+' '+t+' '+str(b)))
        return float(v.pop())
    raise ValueError("Style not recognized")

import sys

for s in sys.stdin:
    s = s.strip()
    if s[0].isdigit():
        style = 'postfix'
    else:
        style = 'prefix'

    print(value(s))
