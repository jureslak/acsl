import unittest
from number_systems import *

class TestNumberSystems(unittest.TestCase):

    def test_convert_to_decimal(self):
        # single digit
        self.assertEqual(convert_to_decimal('1', 13), 1)
        self.assertEqual(convert_to_decimal('9', 11), 9)
        self.assertEqual(convert_to_decimal('0', 35), 0)
        # basic
        self.assertEqual(convert_to_decimal('100', 3), 9)
        self.assertEqual(convert_to_decimal('100', 4), 16)
        self.assertEqual(convert_to_decimal('100', 20), 400)
        self.assertEqual(convert_to_decimal('A', 36), 10)
        self.assertEqual(convert_to_decimal('a1', 32), 321)
        self.assertEqual(convert_to_decimal('bc', 15), 177)
        # large
        self.assertEqual(convert_to_decimal('111000111000', 2), 3640)
        self.assertEqual(convert_to_decimal('4564', 7), 1663)
        self.assertEqual(convert_to_decimal('88888', 9), 59048)
        self.assertEqual(convert_to_decimal('ABCD', 14), 29777)
        self.assertEqual(convert_to_decimal('JJJ', 20), 7999)
        self.assertEqual(convert_to_decimal('test', 36), 1372205)
        # other
        self.assertEqual(convert_to_decimal('10', 10), 10)
        self.assertEqual(convert_to_decimal('1234567890', 10), 1234567890)
        # exceptions
        self.assertRaisesRegex(ValueError,
            "base must be an integer, got type '<class 'str'>' with value '2'", convert_to_decimal, '1', '2')
        self.assertRaisesRegex(ValueError,
            "string must be a string, got type '<class 'int'>' with value '1'", convert_to_decimal, 1, 10)
        self.assertRaisesRegex(ValueError,
            "base must be between 2 and 36, got '1'", convert_to_decimal, '1', 1)
        self.assertRaisesRegex(ValueError,
            "base must be between 2 and 36, got '100'", convert_to_decimal, '1', 100)

    def test_convert_from_decimal(self):
        # single digit
        self.assertEqual(convert_from_decimal(1, 13), '1')
        self.assertEqual(convert_from_decimal(9, 11), '9')
        self.assertEqual(convert_from_decimal(0, 35), '0')
        # basic
        self.assertEqual(convert_from_decimal(9, 3), '100')
        self.assertEqual(convert_from_decimal(16, 4), '100')
        self.assertEqual(convert_from_decimal(400, 20), '100')
        self.assertEqual(convert_from_decimal(10, 36), 'A')
        self.assertEqual(convert_from_decimal(321, 32), 'A1')
        self.assertEqual(convert_from_decimal(177, 15), 'BC')
        # large
        self.assertEqual(convert_from_decimal(3640, 2), '111000111000')
        self.assertEqual(convert_from_decimal(1663, 7), '4564')
        self.assertEqual(convert_from_decimal(59048, 9), '88888')
        self.assertEqual(convert_from_decimal(29777, 14), 'ABCD')
        self.assertEqual(convert_from_decimal(7999, 20), 'JJJ')
        self.assertEqual(convert_from_decimal(1372205, 36), 'TEST')
        # other
        self.assertEqual(convert_from_decimal(10, 10), '10')
        self.assertEqual(convert_from_decimal(1234567890, 10), '1234567890')
        # exceptions
        self.assertRaisesRegex(ValueError,
            "base must be an integer, got type '<class 'str'>' with value '2'", convert_from_decimal, 1, '2')
        self.assertRaisesRegex(ValueError,
            "n must be an integer, got type '<class 'str'>' with value '1'", convert_from_decimal, '1', 10)
        self.assertRaisesRegex(ValueError,
            "base must be between 2 and 36, got '1'", convert_from_decimal, 1, 1)
        self.assertRaisesRegex(ValueError,
            "base must be between 2 and 36, got '100'", convert_from_decimal, 1, 100)

    def test_convert_from_to(self):
        # single digit
        self.assertEqual(convert_from_to('1', 2, 36), '1')
        self.assertEqual(convert_from_to('K', 30, 25), 'K')
        self.assertEqual(convert_from_to('0', 35, 2), '0')
        # basic
        self.assertEqual(convert_from_to('N', 24, 23), '10')
        self.assertEqual(convert_from_to('1010', 2, 16), 'A')
        self.assertEqual(convert_from_to('16', 7, 20), 'D')
        self.assertEqual(convert_from_to('1212', 4, 16), '66')
        # large
        self.assertEqual(convert_from_to('1123001', 4, 16), '16C1')
        self.assertEqual(convert_from_to('ABCD', 15, 7), '211114')
        self.assertEqual(convert_from_to('ABCD', 15, 30), '1ADS')
        self.assertEqual(convert_from_to('test', 36, 16), '14F02D')
        # other
        self.assertEqual(convert_from_to('123', 36, 36), '123')
        self.assertEqual(convert_from_to('456', 8, 8), '456')
        # exceptions
        self.assertRaisesRegex(ValueError,
            "base1 must be an integer, got type '<class 'str'>' with value '2'", convert_from_to, '1', '2', 5)
        self.assertRaisesRegex(ValueError,
            "base2 must be an integer, got type '<class 'str'>' with value '5'", convert_from_to, '1', 2, '5')
        self.assertRaisesRegex(ValueError,
            "string must be a string, got type '<class 'int'>' with value '1'", convert_from_to, 1, 10, 16)
        self.assertRaisesRegex(ValueError,
            "base1 must be between 2 and 36, got '1'", convert_from_to, '1', 1, 10)
        self.assertRaisesRegex(ValueError,
            "base2 must be between 2 and 36, got '100'", convert_from_to, '1', 10, 100)

if __name__ == '__main__':
    unittest.main()
