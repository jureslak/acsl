import sys
import re

class val:
    def __init__(self,s):
        self.len = len(s)
        self.value = int(s,2)
    def __mul__(self, o):
        return '{:0{1}b}'.format(self.value & o.value, self.len)
    def __div__(self, o):
        return '{:0{1}b}'.format(~(self.value & o.value), self.len)
    def __add__(self, o):
        return '{:0{1}b}'.format(self.value ^ o.value, self.len)
    def __lshift__(self, o):
        return '{:0{1}b}'.format(self.value | o.value, self.len)
    def __rshift__(self, o):
        return '{:0{1}b}'.format(~(self.value | o.value), self.len)
    def __lt__(self, o):
        return '{:0{1}b}'.format(self.value << o.value, self.len)
    def __gt__(self, o):
        return '{:0{1}b}'.format(self.value >> o.value, self.len)
    def __invert__(self):
        return '{:0{1}b}'.format(~self.value, self.len)
    def __le__(self, o):
        c = o.value % self.len
        v = self.value
        return '{:0{1}b}'.format(v[c:]+v[:c], self.len)
    def __ge__(self, o):
        c = o.value % self.len
        v = self.value
        return '{:0{1}b}'.format(v[:c]+v[c:], self.len)
    
def findend(s, i):
    s = r = p = s[i:]
    while True:
        for j in ['LSHIFT','RSHIFT','LCIRC','RCIRC']:
            if p.startswith(j):
                p = p[len(j):]
                p = p[p.find(' '):].lstrip()
        if p == r: break
        print (p)
        r = p
    if p[0] == '(':
        c = 0
        for j in range(len(p)):
            if p[j] == '(': c += 1
            if p[j] == ')': c -= 1
            if c == 0: break
        p = p[j+1:]
    return i+len(s)-len(p)

def reformat(s):
    code = dict(zip(['LSHIFT','RSHIFT','LCIRC','RCIRC'],['<','>','<=','>=']))
    for op in ['LSHIFT','RSHIFT','LCIRC','RCIRC']:
        i = s.find(op)
        while i != -1:
            f = s[:i]
            n = s[i:].split()[0]
            n = n[n.find('-')+1:]
            e = findend(s,i)
            m = s[i+len(op)+1+len(n):e]
            print (f, m, n)
            s = f + '(' + m + ' ' + code[op] + ' ' + n + ')' + s[e:]
            i = s.find(op,i+len(op))
    return s

for line in sys.stdin:
    line = line.strip()
    line = re.sub('[01]{5,}',r' val("\g<0>") ',line)
    rep = [['NAND','/'],['NOR','>>'],['AND','*'],['XOR','+'],['OR','<<'],['NOT','~']]
    for i,j in rep:
        line = line.replace(i,j)
    print("Before:",line)
    line = reformat(line)
    print("After:",line)
    print(eval(line))
