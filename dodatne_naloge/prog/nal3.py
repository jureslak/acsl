def odloc(v, d):
    #v je velikost, d je seznam delcev
    p1 =10000000
    p2 =10000000
    if(len(d) == 0):
        return 0
    if(v > d[-1]):
        return 0
    if(v-1 != 0):
        p1 = odloc(v + max(0,v-1),d)
    d = d[:-1]
    p2 =odloc(v,d)
    potez = min(p1,p2) + 1 #ali ga poveca ali pa odstrani najvecjega
    return potez
t = int(raw_input())
for da in range(t):
    veli, n = map(int, raw_input().split())
    delci = map(int, raw_input().split())
    while (len(delci) > 0 and min(delci)<veli):
        for i in delci:
            if(veli > i):
                veli+=i
                delci.remove(i)

    #print veli, delci
    print odloc(veli, sorted(delci))
