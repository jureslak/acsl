import unittest
import doctest

from expression_notation_test import *
from number_systems_test import *
from number_theory_test import *
from fractions_test import *
from graphs_test import *
from matrices_test import *

unittest.main()
