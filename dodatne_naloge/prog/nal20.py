import roman
for da in range(int(raw_input())):
    di = {}
    fl = True
    for dda in range(int(raw_input())):
        kluc, vrednost = raw_input().replace(" ", "").split('=')
        vrednost = vrednost.split('+')
        #print(kluc, vrednost)
        if(len(vrednost) == 1):
            #gleda ce je pravilna rimska stevilka
            try:
                di[kluc] = roman.fromRoman("".join(vrednost))
            except Exception, e:
                print "#%#$#&!"
                fl = False
                continue

        elif(len(vrednost) > 1):
            di[kluc] = vrednost
    #izpisovanje dictionary
    #for i in di.keys():
        #print (str(i) + " =>" + str(di[i]))

    zah = raw_input()
    if(fl):
        while(True):
            for i in di.keys():
                try:
                    #poskusa evaluirati izraz
                    di[str(i)] = di[di[str(i)][0]]+di[di[str(i)][1]]
                    #print di[str(i)]
                except Exception, e:
                    continue
            try:
                #preveri ce smo ze izracunali vrednost
                if(type(di[zah]) == int):
                    print roman.toRoman(di[zah])
                    break
            except Exception, e:
                continue
