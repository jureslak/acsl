# Avtor: Jure Slak
# Python, version: 3.3

scores = {'A':1, 'E':1, 'D':2, 'R':2, 'B':3, 'M':3, 'V':4, 'Y':4, 'J':8, 'X':8}

def value(val, letter): # returns (score, multiplier) par, samo za to da je to nekje posebej
    if val % 3 == 0 and (val//3) % 2 == 1: return 2*scores[letter], 1  # da je v glavnem delu manj solate
    if val % 5 == 0: return 3*scores[letter], 1
    if val % 7 == 0: return scores[letter], 2
    if val % 8 == 0: return scores[letter], 3   # ni nam treba paziti na meje, saj
    return scores[letter], 1                    # besedilo zagotavlja veljavnost podatkov.

b = input().split()
for t in range(5):
    n, d = input().split()
    n = int(n)

    score = 0
    mult = 1
    smer = 1 if d == 'H' else 10    # lahko tudi smer = 1 + 9*(d == 'V'), če se počutte fancy
    for i in range(4):
        s, m = value(n*smer+i, b[i]) # smer naredi to kar je treba, premika nas navzdol
        score += s                   # ali v desno. Potem prištejemo score, in si povečamo
        mult *= m                    # multiplier. Itak bo samo en, lahko bi uporabili tudi max
    print(score*mult)
