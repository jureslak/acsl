x, y, z = [input() for i in range(3)]

def div_(c):
    return c[:4] * 2, c[4:] * 2
def add_(c, n):
    return ''.join((c[:n]*2 + c[n:])[:8])
def sub_(c, n):
    if n == 0: return c
    return ''.join(list(c[n:]) + sorted(c[-n:]))
def uni_(c, d):
    return ''.join(sorted(c[4:]) + sorted(d[:4]))
def int_(c, d):
    return ''.join(sorted(c[:2]+c[6:]) + sorted(d[:2]+d[6:]))
def ali_(c):
    return ''.join(sorted(c)[::-1])

for da in range(10):
    l = input().lower().split()
    last = l[:]
    try:
        while 1:
            last = l[:]
            v = l.pop()
            if v in 'xyz': v = eval(v)
            o = l.pop()
            v2 = ''
            if len(o) == 1 or len(o) == 8:
                v2 = o
                if v2 in 'xyz': v2 = eval(v2)
                o = l.pop()
            if v2:
                v, v2 = v2, v
            if 'uni' in o:
                l.append(uni_(v, v2))
            elif 'int' in o:
                l.append(int_(v, v2))
            elif 'add' in o:
                l.append(add_(v, int(o[-1])))
            elif 'sub' in o:
                l.append(sub_(v, int(o[-1])))
            elif 'div' in o:
                l += list(div_(v))
            elif 'ali' in o:
                l.append(ali_(v))
            if o not in 'uni,int' and v2:
                l.append(v2)
    except Exception as e:
        print(' and '.join(last))
