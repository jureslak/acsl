# code to generate bitstring tasks

import random
import re
import sys

# some settings
bitlen = 5          # how many bits
strcnt = 5         # how many bit strings
opcnt = 9          # how many operators
parenth = True      # use parenthesis
equation = False    # make an equation - not implemented
usesymbols = False  # use symbols for operators

binops = ['AND', 'XOR', 'OR', 'NAND', 'NOR']
binopsprob = [0.27, 0.27, 0.27, 0.095, 0.095]
unops = ['NOT', 'LCIRC', 'RCIRC', 'LSHIFT', 'RSHIFT']
unopsprob = [0.2, 0.2, 0.2, 0.2, 0.2]
shiftprob = [[1,0.35],[2,0.35],[3,0.2],[4,0.10],[5,0.05]]

assert len(binops) == len(binopsprob)
assert len(unops) == len(unopsprob)
# assert sum(binopsprob) == 1
# assert sum(unopsprob) == 1
assert opcnt >= strcnt - 1

def weighted_choice(choices):
    choices = list(choices)
    total = sum(w for c, w in choices)
    r = random.uniform(0, total)
    upto = 0
    for c, w in choices:
        if upto + w > r:
            return c
        upto += w
    raise ValueError("Probabilites messed up")

def random_string():
    return '{0:0{1}b}'.format(random.randint(0, 1 << (bitlen-1)),bitlen)

def format(op):
    if op in unops:
        if op[1:] == 'CIRC':
            op += '-'+str(random.randint(1,bitlen*4))
        elif op[1:] == 'SHIFT':
            op += '-'+str(weighted_choice(shiftprob))
        op += ' '
    elif op in binops:
        op = ' ' + op + ' '
    return op

def add_unary(a, b=None):
    global opsleft, stringsleft
    while opsleft >= stringsleft and opsleft > 0:
        p = stringsleft / opsleft
        if weighted_choice([[0,p],[1,1-p]]):
            if random.randint(0,1) or b is None:
                a = format(weighted_choice(zip(unops,unopsprob)))+a
                if parenth: a = '(' + a + ')'
            else:
                b = format(weighted_choice(zip(unops,unopsprob)))+b
                if parenth: b = '(' + b + ')'
            opsleft -= 1
        else: break
    return a,b
    
def merge(a, b):
    global stringsleft, opsleft
    opsleft -= 1

    if any(i in a for i in binops+unops) and parenth: a = '(' + a + ')'
    if any(i in b for i in binops+unops) and parenth: b = '(' + b + ')'

    a, b = add_unary(a, b)

    if random.randint(0,1):
        a, b = b, a

    return a + format(weighted_choice(zip(binops,binopsprob))) + b

def texify(s):
    if usesymbols:
        s = re.sub(' AND ',r' \;\&\; ',s)
        s = re.sub(' XOR ',r' \;\oplus\; ',s)
        s = re.sub('NOT ',r'\sim',s)
        s = re.sub(' OR ',r' \;|\; ',s)
        s = re.sub('SHIFT-',r'S',s)
        s = re.sub('CIRC-',r'C',s)
    ops = '|'.join(i+'(-[1-9]+)?' for i in unops) + '|' + '|'.join(binops)+'|[RL][CS][0-9]+'
    s = re.sub(r'\s*('+ops+')\s*',r'\\text{\g<0>}',s)
    return s

stringsleft = strcnt
opsleft = opcnt

if equation == False:
    s = random_string()
    stringsleft -= 1
    for i in range(strcnt-1):
        s = merge(s, random_string())
        stringsleft -= 1
    s,_ = add_unary(s)

    print (s)
#     usesymbols = random.choice([True, False])
#     print (s, file=sys.stderr)
    print (texify(s),'\\\\')
