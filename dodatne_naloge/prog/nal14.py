st, n = raw_input().split()
n = int(n)
r = ''
for i in range(0,len(st), n):
    c = 0
    for j in range(i,min(i+n, len(st))):
        #print st[j]
        c += ord(st[j])
    #print c, min(i+n, len(st))
    t = chr(c/min(n, len(st)-i))
    r += min(n, len(st)-i)*t
print r
