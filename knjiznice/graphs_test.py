import unittest
from graphs import *

class TestGraphs(unittest.TestCase):
    def test_dijkstra(self):
        G = {
            0: {1: 4, 2: 3, 3: 1},
            1: {0: 1, 2: 3},
            2: {1: 2, 3: 6, 0: 5},
            3: {1: 4, 0: 5, 2: 7},
        }
        self.assertEqual(shortest_path(G, 0, 1), (4, [0, 1]))
        G = {
            0: {2: 3, 3: 2},
            1: {0: 1, 2: 3},
            2: {3: 6, 0: 5},
            3: {1: 4, 0: 5, 2: 7},
        }
        self.assertEqual(shortest_path(G, 0, 1), (6, [0, 3, 1]))
        G = {
            0: {2: 3, 3: 1},
            1: {0: 1, 2: 3},
            2: {3: 6, 0: 5},
            3: {0: 5, 2: 7},
        }
        self.assertEqual(shortest_path(G, 0, 1), (0, []))

if __name__ == '__main__':
    unittest.main()
