"""Module containing useful functions for converting numbers in different bases."""

DIGITS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def convert_to_decimal(string, base):
    """Converts given string in base b to a decimal number. Base must be between 2
    and 36, inclusive. Letters in input string may be uppercase or lowercase.
    >>> convert_to_decimal('1010', 2)
    10
    >>> convert_to_decimal('1A', 20)
    30
    """
    if not isinstance(base, int):
        raise ValueError("base must be an integer, got type '{}' with value '{}'".format(type(base), base))
    if not isinstance(string, str):
        raise ValueError("string must be a string, got type '{}' with value '{}'".format(type(string), string))
    if base < 2 or base > 36:
        raise ValueError("base must be between 2 and 36, got '{}'".format(base))
    return int(string, base)

def convert_from_decimal(n, base):
    """Converts given decimal integer n to a base b. Base must be between 2 and 36,
    inclusive. All letters in returned string are uppercase.
    >>> convert_from_decimal(37, 2)
    '100101'
    >>> convert_from_decimal(11, 30)
    'B'
    """
    if not isinstance(base, int):
        raise ValueError("base must be an integer, got type '{}' with value '{}'".format(type(base), base))
    if not isinstance(n, int):
        raise ValueError("n must be an integer, got type '{}' with value '{}'".format(type(n), n))
    if base < 2 or base > 36:
        raise ValueError("base must be between 2 and 36, got '{}'".format(base))
    if n == 0: return '0'
    digits = ''
    while n:
        digits += DIGITS[n % base]
        n //= base
    return digits[::-1]

def convert_from_to(string, base1, base2):
    """Converts given string in base1 to a base2. Both bases must be between
    2 and 36, inclusive. All letters in returned string are uppercase.
    >>> convert_from_to('1201210', 3, 9)
    '1653'
    >>> convert_from_to('2301', 5, 8)
    '506'
    """
    if not isinstance(base1, int):
        raise ValueError("base1 must be an integer, got type '{}' with value '{}'".format(type(base1), base1))
    if not isinstance(base2, int):
        raise ValueError("base2 must be an integer, got type '{}' with value '{}'".format(type(base2), base2))
    if not isinstance(string, str):
        raise ValueError("string must be a string, got type '{}' with value '{}'".format(type(string), string))
    if base1 < 2 or base1 > 36:
        raise ValueError("base1 must be between 2 and 36, got '{}'".format(base1))
    if base2 < 2 or base2 > 36:
        raise ValueError("base2 must be between 2 and 36, got '{}'".format(base2))
    n = int(string, base1)
    if n == 0: return '0'
    digits = ''
    while n:
        digits += DIGITS[n % base2]
        n //= base2
    return digits[::-1]

def convert_fraction(stevec, imenovalec, base):
    """Converts given fraction in base 10 to decimal number in base `base`.
    >>> convert_fraction(5, 3, 2)
    '1.(10)'
    >>> convert_fraction(5, 3, 3)
    '1.2'
    >>> convert_fraction(5, 3, 10)
    '1.(6)'
    >>> convert_fraction(37, 90, 10)
    '0.4(1)'
    >>> convert_fraction(13*15+14, 15, 16)
    'D.(E)'
    """
    q, r = divmod(stevec, imenovalec)
    result = convert_from_decimal(q, base)
    if r == 0: return result

    decimalke = ''
    result += '.'
    mesto = 0;
    ostanki = dict()
    ostanki[r] = mesto;
    while r != 0:  # pisno deljenje
        mesto += 1;
        r *= base;
        decimalke += DIGITS[r // imenovalec];
        r %= imenovalec;
        if  r in ostanki:  # periodicno
            result += decimalke[:ostanki[r]]
            result += '('
            result += decimalke[ostanki[r]:]
            result += ')'
            return result
        ostanki[r] = mesto
    result += decimalke;
    return result

if __name__ == '__main__':
    import doctest
    doctest.testmod()
