"""Functions for working with matrices."""

def transpose(matrix_in):
    m = len(matrix_in)
    n = len(matrix_in[0])
    matrix = [[0 for i in range(m)] for j in range(n)]

    for i in range(m):
        for j in range(n):
            matrix[j][i] = matrix_in[i][j]

    return matrix

# Rotating left
def rotate_90(matrix_in):
    m = len(matrix_in)
    n = len(matrix_in[0])
    matrix = [[0 for i in range(m)] for j in range(n)]

    for i in range(m):
        for j in range(n):
            matrix[n-j-1][i] = matrix_in[i][j]

    return matrix

def flip_lr(matrix_in):
    matrix = []
    for i in range(len(matrix_in)):
        matrix.append(matrix_in[i][::-1])
    return matrix

def flip_ud(matrix_in):
    return matrix_in[::-1]
