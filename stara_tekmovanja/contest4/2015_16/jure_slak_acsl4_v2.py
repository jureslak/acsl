import re
s = input().replace('#', '').split(' ')
for _ in range(5):
    pattern = input()
    l = [x for x in s if re.match(pattern, x) is not None ]
    print(' '.join(l) or "NONE")
