from collections import defaultdict

def A(x):
    return x == 0
def B(x):
    return x == 1 or x == 2 or x == 4 or x == 0
def C(x):
    return x == 7
def D(x):
    return x != 7

for da in range(5):
    n, *l, last = input().split()
    n = int(n)
    layer2 = defaultdict(int)
    for i in range(n):
        gate, out = l[2*i: 2 + 2*i]
        o = eval(gate[0] + '(' + gate[1] + ')')
        for j in out:
            layer2[j] = layer2[j]*2 + o
    o = 0
    s = ''
    if len(layer2.keys()) == 1:
        k = list(layer2.keys())
        s = '{:0>3b}'.format(int(layer2[k[0]]))
        o = layer2[k[0]]
    else:
        for i in sorted(layer2.keys()):
            q = eval(i + '(' + str(layer2[i]) + ')')
            s += str(int(q))
            o = o*2 + q
    print(s)
    print(int(eval(last + '(' + str(o) + ')')))
