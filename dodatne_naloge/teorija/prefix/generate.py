import random
import sys

# style = "infix"
style = "prefix"  # select style
# style = "postfix"
opcnt = 10     # how many operators
intstart = 0   # integer value range
intstop  = 8   # to use
mode = "evaluate"  
# mode = "convert" # select mode

# avaliable list of operators
binary = "+-*/^"
unary = []


if mode == "convert":
    unary = ['sqrt',"sin","exp",'!']
    binary += '&%#'

def weighted_choice(choices):
    choices = list(choices)
    total = sum(w for c, w in choices)
    r = random.uniform(0, total)
    upto = 0
    for c, w in choices:
        if upto + w > r:
            return c
        upto += w
    raise ValueError("Probabilites messed up")

def texify(s):
    return '$'+s.replace(' ','~').replace('^','\\uparrow').replace('#','\\#').replace('%','\\%').replace('&','\\&').replace('sin','\\sin').replace('exp','\\exp').replace('sqrt','\\sqrt{\ }')+'$'

def value(s):
    s = s.replace('^','**')
    if style == 'infix': return eval(s)
    if style == 'postfix':
        v = []
        for t in s.split():
            try: v.append(float(t))
            except ValueError:
                a, b = v.pop(), v.pop()
                v.append(eval(str(b)+' '+t+' '+str(a)))
        return int(v.pop())
    if style == 'prefix':
        v = []
        for t in reversed(s.split()):
            try: v.append(float(t))
            except ValueError:
                a, b = v.pop(), v.pop()
                v.append(eval(str(a)+' '+t+' '+str(b)))
        return float(v.pop())
    raise ValueError("Style not recognized")

def merge(a, b, op=None):
    if op is None:
        op = b
        if style == "prefix" or style == "infix":
            return op + ' ' + a
        if style == "postfix":
            return a + ' ' + op
    else:
        if style == "prefix":
            return op + ' ' + a + ' ' + b 
        if style == "infix":
            return '(' + a + ' ' + op + ' ' + b + ')'
        if style == 'postfix':
            return a + ' ' + b + ' ' + op
    raise ValueError("Style not recognized")

def gen(n):
    if n == 0:
        if mode == "evaluate":
            return str(random.randint(intstart, intstop))
        elif mode == "convert":
            return str(chr(random.randint(ord('A'),ord('Z'))))
        else: raise ValueError("Unrecognised mode")

    k = random.randint(0, n-1)
    if weighted_choice([[1, len(binary)],[0, len(unary)]]):
        a = gen(n-k-1)
        b = gen(k)
        if mode == "evaluate":
            va = value(a)
            vb = value(b)
            i = 0
            while True:
                op = random.choice(binary)
                i += 1
                if i >= 100: op = '+'; break
                if op == '/' and vb == 0: continue
                if op == '^' and va == 0 and vb <= 0: continue
                if op == '/' and va % vb != 0: continue
                if abs(eval(str(va)+op.replace('^','**')+str(vb))) > 100: continue
                break
        elif mode == "convert":
            op = random.choice(binary)
        else: raise ValueError("Unrecognised mode")
        return merge(a, b, op)
    else:
        a = gen(n-1)
        op = random.choice(unary)
        return merge(a, op)

a = gen(opcnt)
# print(texify(a))
print(a)
if mode == 'evaluate':
    print(value(a),file=sys.stderr)
