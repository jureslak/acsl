def check(d):
    c = d[-1]
    s = luhn(d)
    return c == s

def luhn(d):
    d = d[::-1]
    for i in range(1, len(d), 2):
        d[i] *= 2
        if d[i] > 9: d[i] = 1 + d[i] % 10
    s = (sum(d[1:]) * 9) % 10
    return s

for da in range(4):
    d = [int(i) for i in input()]
    if check(d): print('VALID')
    else: print(luhn(d))

for da in range(2):
    d = [int(i) for i in input()]
    for i in range(len(d)):
        d[i] = (d[i] - 1) % 10
        if check(d):
            print(''.join([str(i) for i in d]))
            break
        d[i] = (d[i] + 2) % 10
        if check(d):
            print(''.join([str(i) for i in d]))
            break
        d[i] = (d[i] - 1) % 10

for da in range(2):
    d = [int(i) for i in input()]
    for i in range(len(d)-1):
        d[i], d[i+1] = d[i+1], d[i]
        if check(d):
            print(''.join([str(i) for i in d]))
            break
        d[i], d[i+1] = d[i+1], d[i]

for da in range(2):
    d = [int(i) for i in input()]
    for i in range(len(d)-1):
        if d[i] == d[i+1]:
            d[i] = d[i+1] = (d[i] - 1) % 10
            if check(d):
                print(''.join([str(i) for i in d]))
                break
            d[i] = d[i+1] = (d[i] + 2) % 10
            if check(d):
                print(''.join([str(i) for i in d]))
                break
            d[i] = d[i+1] = (d[i] - 1) % 10
