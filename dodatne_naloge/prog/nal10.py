n,m  = map(int,raw_input().split())
sahovnica = [[0 for i in range(n)] for j in range(n)]

def polozi(x,y):
    if(x>=0 and x<n and y >=0 and y<n):
        sahovnica[x][y] = 1

def ver_hor(x,y):
    for i in range(x+1,n):
        sahovnica[i][y] = 1
    for i in range(0,x):
        sahovnica[i][y] = 1
    for i in range(0,y):
        sahovnica[x][i] = 1
    for i in range(y+1, n):
        sahovnica[x][i] = 1

def diag(x,y):
    for i in range(0, n):
        for j in range(0, n):
            if(i!=x and j!=y and i == j):
                sahovnica[i][j] = 1
                sahovnica[-i][j] = 1

def Q(x,y):
    ver_hor(x,y)
    diag(x,y)
def R(x,y):
    ver_hor(x,y)
def B(x,y):
    diag(x,y)
def A(x,y):
    polozi(x+1,y)
    polozi(x-1,y)
    polozi(x+1,y+1)
    polozi(x+1,y-1)
    polozi(x-1,y+1)
    polozi(x-1,y-1)
    polozi(x,y+1)
    polozi(x,y-1)
def P(x,y):
    polozi(x+1,y+1)
    polozi(x+1,y-1)
def K(x,y):
    polozi(x+2,y+1)
    polozi(x+1,y+2)
    polozi(x+2,y-1)
    polozi(x+1,y-2)
    polozi(x-2,y+1)
    polozi(x-1,y+2)
    polozi(x-2,y-1)
    polozi(x-1,y-2)

for da in range(m):
    x,y,tipe = raw_input().split()
    e = tipe + "(" + x + "," + y + ")"
    eval(e)
rez = 0

print sahovnica
for i in sahovnica:
    print i
    rez+=i.count(0)

print rez
