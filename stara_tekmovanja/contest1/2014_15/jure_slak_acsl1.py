# Jure Slak
# Gimanzija Vič
# Intermediate 3

dist = [450, 140, 120, 320, 250, 80]
poraba = {'C': 28, 'M': 25, 'F': 22, 'V': 20}
hitrost = {'I': 65, 'H': 60, 'M': 55, 'S': 45}

for i in range(5):
    l = input().split()
    m1, m2, avto, cesta = l[:4]
    cena = float(l[-1])
    m1, m2 = sorted(ord(x) - ord('A')  for x in [m1, m2])

    d = sum(dist[m1:m2])
    bencin = d / poraba[avto] * cena
    cas = d / hitrost[cesta]
    print("{} {:0>2d}:{:02.0f} ${:.2f}".format(d, int(cas), (cas - int(cas)) * 60, bencin))
