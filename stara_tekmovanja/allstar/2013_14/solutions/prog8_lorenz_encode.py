code = {'A': '11000',
        'B': '10011',
        'C': '01110',
        'D': '10010',
        'E': '10000',
        'F': '10110',
        'G': '01011',
        'H': '00101',
        'I': '01100',
        'J': '11010',
        'K': '11110',
        'L': '01001',
        'M': '00111',
        'N': '00110',
        'O': '00011',
        'P': '01101',
        'Q': '11101',
        'R': '01010',
        'S': '10100',
        'T': '00001',
        'U': '11100',
        'V': '01111',
        'W': '11001',
        'X': '10111',
        'Y': '10101',
        'Z': '10001',
        '+': '11011',
        '/': '00000',
        '9': '00100',
        '8': '11111',
        '4': '01000',
        '3': '00010'}

def next_key1(num):
    num %= 14
    return chr(ord("A") + num)

def next_key2(num):
    num %= 4
    if num < 2: return "A"
    return "B"

for x in code:
    code[x] = int(code[x], 2)
inverse_code = {}
for x in code:
    inverse_code[code[x]] = x


for neki in range(10):
    pos1, pos2, string = input().split()
    pos1 = int(pos1) -1
    pos2 = int(pos2) -1
    result = ""
    for znak in string:
        result += inverse_code[code[znak] ^ code[next_key1(pos1)] ^ code[next_key2(pos2)]]
        pos1 += 1
        pos2 += 1
    print(result)
