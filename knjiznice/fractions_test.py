import unittest
from fractions import *

class TestNumberSystems(unittest.TestCase):

    def test_convert_from_fraction(self):
        # whole numbers
        self.assertEqual(convert_from_fraction(1, 1), ('1', ''))
        self.assertEqual(convert_from_fraction(7, 7), ('1', ''))
        self.assertEqual(convert_from_fraction(9, 3), ('3', ''))
        self.assertEqual(convert_from_fraction(289, 17), ('17', ''))
        # no repeating decimal
        self.assertEqual(convert_from_fraction(1, 4), ('0.25', ''))
        self.assertEqual(convert_from_fraction(7, 20), ('0.35', ''))
        self.assertEqual(convert_from_fraction(123, 100), ('1.23', ''))
        # one decimal
        self.assertEqual(convert_from_fraction(1, 3), ('0.', '3'))
        self.assertEqual(convert_from_fraction(5, 3), ('1.', '6'))
        self.assertEqual(convert_from_fraction(13, 9), ('1.', '4'))
        self.assertEqual(convert_from_fraction(5, 600), ('0.008', '3'))
        self.assertEqual(convert_from_fraction(5, 6), ('0.8', '3'))
        # more decimals
        self.assertEqual(convert_from_fraction(1, 7), ('0.', '142857'))
        self.assertEqual(convert_from_fraction(50, 19), ('2.', '631578947368421052'))
        self.assertEqual(convert_from_fraction(1234, 9999), ('0.', '1234'))
        self.assertEqual(convert_from_fraction(103, 101), ('1.', '0198'))
        # !!!               wolfram alpha says this:      ('1.0', '1980')
        self.assertEqual(convert_from_fraction(1, 61),
            ('0.', '016393442622950819672131147540983606557377049180327868852459'))
        # exceptions
        self.assertRaisesRegex(ValueError,
            "a must be an integer, got type '<class 'str'>' with value '2'", convert_from_fraction, '2', 2)
        self.assertRaisesRegex(ValueError,
            "b must be an integer, got type '<class 'float'>' with value '2.3'", convert_from_fraction, 1, 2.3)
        self.assertRaisesRegex(ZeroDivisionError,
            "fraction denumerator must not be 0.", convert_from_fraction, 1, 0)


if __name__ == '__main__':
    unittest.main()
