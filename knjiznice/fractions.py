"""Module containing useful functions for converting fractions to decimals and vice versa."""

def convert_from_fraction(a, b):
    """Converts fraction to decimal number. Integers a and b represent a fraction
    with numerator a and denumerator b. Returns tuple of two strings, where second
    string repesents repeating decimals at the end of first string number.
    >>> convert_from_fraction(1, 3)
    ('0.', '3')
    >>> convert_from_fraction(10, 4)
    ('2.5', '')
    >>> convert_from_fraction(1, 1)
    ('1', '')
    """
    if not isinstance(a, int):
        raise ValueError("a must be an integer, got type '{}' with value '{}'".format(type(a), a))
    if not isinstance(b, int):
        raise ValueError("b must be an integer, got type '{}' with value '{}'".format(type(b), b))
    if b == 0: raise ZeroDivisionError("fraction denumerator must not be 0.")
    remainders = []
    result = str(a // b)
    a %= b
    a *= 10
    if a == 0: return result, ''
    result += '.'
    while a != 0 and a not in remainders:
        remainders.append(a)
        result += str(a // b)
        a %= b
        a *= 10
    if a % b != 0:
        index = remainders.index(a)
        return result[:index - len(remainders)], result[index - len(remainders):]
    else:
        return result, ''

if __name__ == '__main__':
    import doctest
    doctest.testmod()
