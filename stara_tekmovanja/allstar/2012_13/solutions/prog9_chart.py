def to_num(a):
    return ord(a) - ord('A')

m = [[0 for j in range(5)] for i in range(5)]

for i in range(5):
    m[i][i] = 1

for i in range(4):
    a, q = input().split()
    a, b = [to_num(i) for i in a]
    m[a][b] = float(q)
    m[b][a] = 1 / float(q)

for da in range(25):
    for i in range(5):
        for j in range(5):
            if m[i][j] and not m[j][i]:
                m[j][i] = 1/m[i][j]
            for k in range(5):
                if m[i][j] and m[j][k] and not m[i][k]:
                    m[i][k] = m[i][j] * m[j][k]

for da in range(10):
    a, b = [to_num(i) for i in input()]
    print('{:.2f}'.format(m[a][b]))
