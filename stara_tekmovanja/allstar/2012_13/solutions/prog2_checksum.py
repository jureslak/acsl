def check(d):
    d = d[::-1]
    for i in range(1, len(d), 2):
        d[i] *= 2
        if d[i] > 9: d[i] = 1 + d[i] % 10
    d = d[::-1]
    c = d[-1]
    s = (sum(d[:-1]) * 9) % 10
    return c == s

for da in range(5):
    d = [int(i) for i in input()]
    if check(d[:]):
        print('VALID')
    else:
        for i in range(len(d)):
            d[i] = (d[i] - 1) % 10
            if check(d):
                print(''.join([str(i) for i in d]))
                break
            d[i] = (d[i] + 2) % 10
            if check(d):
                print(''.join([str(i) for i in d]))
                break
            d[i] = (d[i] - 1) % 10
