# Avtor: Jure Slak
# Python, version: 3.3

for i in range(5):
    f, v = input().split()

    dollar   = '$' if f[0] == '$' else ''
    comma    = ',' if ',' in f else ''
    decimals = len(f) - f.index('.') - 1 if '.' in f else 0

    s = dollar + '{0:{1}.{2}f}'.format(float(v),comma,decimals)
    
    width = len(f) - s.count(',') + f.count(',') ## dela tudi brez tega, samo ni prav,
    print('{0:*>{1}s}'.format(s,width))          ## dali so slabe test case
