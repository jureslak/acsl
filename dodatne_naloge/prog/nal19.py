from numpy import *
n = int(raw_input())
original = zeros((n,n), dtype=str)
changed = zeros((n,n),dtype=str)
for i in range(n):
    bes = raw_input()
    for j in range(n):
        original[i][j] = bes[j]

for i in range(n):
    bes = raw_input()
    for j in range(n):
        changed[i][j] = bes[j]
fl = True
#rotira v nasprotni smeri urinega kazalca zato rabis 3 da dobi za 90 stopinj
if (rot90(original,3) == changed).all():
    print 1
elif (rot90(original,2) == changed).all():
    print 2
elif (rot90(original,1) == changed).all():
    print 3
elif (fliplr(original) == changed).all():
    print 4
elif ((rot90(fliplr(original),3) == changed).all() or (rot90(fliplr(original),1) == changed).all()
      or (rot90(fliplr(original),2) == changed).all()):
    print 5
elif (original == changed):
    print 6
else:
    print 7
