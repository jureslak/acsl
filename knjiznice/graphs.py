"""Module containing functions for working with graphs."""

def shortest_path(G, s, t):
    """Finds shortest path from s to t in graph G with positive weights.
    Graph is given as dict of dicts. Returned is the total weight of the path as
    well as the nodes along the path, including s as the first and t as the last
    node. If you wish a simple BFS instead of dijkstra, you can set all weights
    to 1.  You can also use deque instread of heap in that case.
    >>> G = { 0: {2: 3, 3: 1}, 1: {0: 1, 2: 3}, 2: {3: 6, 0: 5}, 3: {1: 4, 0: 5, 2: 7}, }
    >>> shortest_path(G, 0, 1)
    (5, [0, 3, 1])
    """
    from heapq import heappop, heappush
    q = [(0, s, s)]  # dist, current, previous
    visited = set()
    prev = dict()
    while q:
        d, c, p = heappop(q)
        if c in visited: continue
        visited.add(c)
        prev[c] = p
        if c == t:  # found the goal
            break
        for v, w in G[c].items():
            heappush(q, (d+w, v, c))
    else:
        return 0, []  # could not reach the goal
    path = [t]
    while prev[path[-1]] != path[-1]:
        path.append(prev[path[-1]])
    path.reverse()
    return d, path

if __name__ == '__main__':
    import doctest
    doctest.testmod()
