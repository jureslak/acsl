from collections import defaultdict

# mabye graphs weren't a good idea

def last(x): return x[-1]

def shortest_path():
    heap = set()  # don't konw how to implement heap with custom sorting method
    visited = []
    ax, ay, bx, by = [int(i)-1 for i in input().split()]
    b = [bx, by]
    heap.add((ax, ay, 0))
    for qqq in range(100):
        try:
            qq = min(heap, key=last)
            heap.remove(qq)
            *t, dis = qq
        except: print('ne dela'); break
        if t == b:
            if dis == int(dis): print(int(dis))
            else: print('{:.1f}'.format(dis))
            break
        if t in visited: continue
        visited.append(t)
        s = graph[str(t)]
        for j in s:
            a1, a2, d = j
            d += dis
            heap.add((a1, a2, d))

graph = defaultdict(set)

for i in range(5):
    for j in range(0, 5, 2):
        graph[str([i, j])].add((i+1, j, 1))
        graph[str([i+1, j+1])].add((i, j+1, 1))
        graph[str([j, i])].add((j, i+1, 1))
        graph[str([j+1, i+1])].add((j+1, i, 1))

for i in range(5): shortest_path()

graph[str([0, 1])].add((1, 2, 1.4)); graph[str([1, 2])].add((0, 1, 1.4))
graph[str([1, 2])].add((2, 3, 1.4)); graph[str([2, 3])].add((1, 2, 1.4))

graph[str([1, 1])].add((2, 2, 1.4)); graph[str([2, 2])].add((1, 1, 1.4))
graph[str([2, 2])].add((3, 3, 1.4)); graph[str([3, 3])].add((2, 2, 1.4))
graph[str([3, 3])].add((4, 4, 1.4)); graph[str([4, 4])].add((3, 3, 1.4))

graph[str([0, 5])].add((1, 4, 1.4)); graph[str([1, 4])].add((0, 5, 1.4))
graph[str([1, 4])].add((2, 3, 1.4)); graph[str([2, 3])].add((1, 4, 1.4))
graph[str([2, 3])].add((3, 2, 1.4)); graph[str([3, 2])].add((2, 3, 1.4))
graph[str([3, 2])].add((4, 1, 1.4)); graph[str([4, 1])].add((3, 2, 1.4))

for i in range(5): shortest_path()
