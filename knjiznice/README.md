# Knjižnice

Uporabne funkcije za tekmovanje. Trenutno obsegajo:

#### `number_systems.py`
  - `convert_to_decimal(string, base)`: converts string in given base to decimal
  - `convert_from_decimal(n, base)`: coverts decimal integer n to a base
  - `convert_from_to(string, base1, base2)`: converts string in base1 to base2

#### `number_theory.py`
  - `divisors(n)`: returns a list of divisors
  - `primes(n)`: returns a list of primes
  - `factorize(n)`: return a list of factors
  - `is_prime(p)`: returns True is p is prime

#### `expression_notation.py`
  - `evaluate_prefix(s)`: evaluates prefix notation expression
  - `evaluate_infix(s)`: evaluates infix notation expression :)
  - `evaluate_postfix(s)`: evaluates postfix notation expression
  - `convert_prefix_to_postfix(s)`: converts prefix expression to postfix expression
  - `convert_prefix_to_infix(s)`: converts prefix expression to infix expression
  - `convert_postfix_to_prefix(s)`: converts postfix expression to postfix expression
  - `convert_postfix_to_infix(s)`: converts prefix expression to postfix expression

#### `fractions.py`
  - `convert_from_fraction(a, b)`: converts fraction to recurring decimal
  - TODO: `convert_to_fraction`: converts recurring decimal to fraction

TODO more stuff :)

You can run doctests by running the module. You can run the unit tests by running module's test
file. You can run all tests, by running
```
nosetests --with-doctest
```
Note that you need to have `python-nose` installed.

Avtorji: Matej Marinko, Jure Slak
