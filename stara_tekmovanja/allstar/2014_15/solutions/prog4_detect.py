from itertools import permutations

def decode(mask, string):
    result = 0
    for index in range(len(string)):
        if string[index] == 1:
            result += int(mask[index])
    return result

#print(decode("01236", "01010"))

for asdfasdfasd in range(10):
    mask, x, k = input().split()
    x, k  = int(x), int(k)
    zerosandones = []
    for i in range(x):
        zerosandones.append(1)
    for i in range(len(mask)-x):
        zerosandones.append(0)

    valid_results = []
    allcombinations = set(permutations(zerosandones))
    for item in allcombinations:
        if decode(mask, item) == k:
            valid_results.append(item)

    if len(valid_results) == 0:
        print("NONE")
    else:
        print(" ".join(''.join(map(str,x)) for x in valid_results))
