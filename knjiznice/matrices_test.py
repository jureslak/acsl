import unittest
from matrices import *

class TestMatrices(unittest.TestCase):
    def test_transpose(self):
        self.assertEqual(transpose([[1, 2]]), [[1], [2]])
        self.assertEqual(transpose([[1, 2], [3, 4]]), [[1, 3], [2, 4]])
        self.assertEqual(transpose([[1, 2], [3, 4], [5, 6]]), [[1, 3, 5], [2, 4, 6]])
        self.assertEqual(transpose([[1], [2], [3], [4]]), [[1, 2, 3, 4]])
        self.assertEqual(transpose([[3, 5, 1], [2, 9, 8], [4, 7, 6]]),
                                    [[3, 2, 4], [5, 9, 7], [1, 8, 6]])

    def test_rotate_90(self):
        self.assertEqual(rotate_90([[1, 2]]), [[2], [1]])
        self.assertEqual(rotate_90([[1, 2], [3, 4]]), [[2, 4], [1, 3]])
        self.assertEqual(rotate_90([[1, 2], [3, 4], [5, 6]]), [[2, 4, 6], [1, 3, 5]])
        self.assertEqual(rotate_90([[1], [2], [3]]), [[1, 2, 3]])
        self.assertEqual(rotate_90([[2, 6, 3], [9, 4, 7], [8, 1, 5]]),
                                    [[3, 7, 5], [6, 4, 1], [2, 9, 8]])

    def test_flip_lr(self):
        self.assertEqual(flip_lr([[1, 2]]), [[2, 1]])
        self.assertEqual(flip_lr([[1], [2]]), [[1], [2]])
        self.assertEqual(flip_lr([[1, 2, 3]]), [[3, 2, 1]])
        self.assertEqual(flip_lr([[1, 2, 3, 4], [1, 2, 1, 2]]), [[4, 3, 2, 1], [2, 1, 2, 1]])
        self.assertEqual(flip_lr([[6, 9, 2], [1, 7, 8], [3, 5, 4]]),
                                    [[2, 9, 6], [8, 7, 1], [4, 5, 3]])

    def test_flip_ud(self):
        self.assertEqual(flip_ud([[1, 2]]), [[1, 2]])
        self.assertEqual(flip_ud([[1], [2]]), [[2], [1]])
        self.assertEqual(flip_ud([[1, 2], [3, 4]]), [[3, 4], [1, 2]])
        self.assertEqual(flip_ud([[1, 2], [3, 4], [5, 6]]), [[5, 6], [3, 4], [1, 2]])
        self.assertEqual(flip_ud([[1], [2], [3], [4], [5]]), [[5], [4], [3], [2], [1]])

if __name__ == '__main__':
    unittest.main()
