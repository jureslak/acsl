import unittest
from number_theory import *

class TestNumberTheory(unittest.TestCase):

    # vse metode, ki imajo teste, se začnejo z `test_`
    def test_divisors(self):
        # small
        self.assertEqual(divisors(1), [1])
        self.assertEqual(divisors(2), [1, 2])
        self.assertEqual(divisors(3), [1, 3])
        self.assertEqual(divisors(4), [1, 2, 4])
        self.assertEqual(divisors(6), [1, 2, 3, 6])
        self.assertEqual(divisors(7), [1, 7])
        self.assertEqual(divisors(8), [1, 2, 4, 8])
        self.assertEqual(divisors(9), [1, 3, 9])

        # perfect squares
        self.assertEqual(divisors(64), [1, 2, 4, 8, 16, 32, 64])
        self.assertEqual(divisors(2401), [1, 7, 49, 343, 2401])
        self.assertEqual(divisors(15367), [1, 11, 121, 127, 1397, 15367])

        # large
        self.assertEqual(divisors(1813312),
            [1, 2, 4, 8, 16, 29, 32, 58, 64, 116, 232, 464, 928, 977,
             1856, 1954, 3908, 7816, 15632, 28333, 31264, 56666, 62528,
             113332, 226664, 453328, 906656, 1813312])

        # exceptions
        self.assertRaisesRegex(ValueError,
            "n must be an integer, got type '<class 'str'>' with value '123'", divisors, "123")
        self.assertRaisesRegex(ValueError,
            r"n must be an integer, got type '<class 'list'>' with value '\[4\]'", divisors, [4])
        self.assertRaisesRegex(ValueError,
            "n must be an integer, got type '<class 'float'>' with value '12.3'", divisors, 12.3)
        self.assertRaisesRegex(ValueError, "n must be positive, got '-1'", divisors, -1)
        self.assertRaisesRegex(ValueError, "n must be positive, got '-5'", divisors, -5)
        self.assertRaisesRegex(ValueError, "n must be positive, got '-143134'", divisors, -143134)

    def test_primes(self):
        # simple
        self.assertEqual(primes(5), [2, 3])
        self.assertEqual(primes(6), [2, 3, 5])
        self.assertEqual(primes(7), [2, 3, 5])
        self.assertEqual(primes(8), [2, 3, 5, 7])
        # small
        self.assertEqual(primes(1), [])
        self.assertEqual(primes(2), [])
        self.assertEqual(primes(3), [2])
        # medium
        self.assertEqual(primes(100), [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
            59, 61, 67, 71, 73, 79, 83, 89, 97])
        self.assertEqual(primes(200), [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
            59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149,
            151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199])
        # large
        self.assertEqual(primes(907), [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
            59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149,
            151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239,
            241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347,
            349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443,
            449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563,
            569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659,
            661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773,
            787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887])
        # exceptions
        self.assertRaisesRegex(ValueError,
            "n must be an integer, got type '<class 'str'>' with value '100'", primes, "100")
        self.assertRaisesRegex(ValueError,
            "n must be an integer, got type '<class 'float'>' with value '1.32'", primes, 1.32)
        self.assertRaisesRegex(ValueError, "n must be positive, got '0'", primes, 0)
        self.assertRaisesRegex(ValueError, "n must be positive, got '-1389'", primes, -1389)

    def test_is_prime(self):
        # small
        self.assertEqual(is_prime(1), False)
        self.assertEqual(is_prime(2), True)
        self.assertEqual(is_prime(3), True)
        self.assertEqual(is_prime(4), False)
        self.assertEqual(is_prime(5), True)
        self.assertEqual(is_prime(6), False)
        self.assertEqual(is_prime(7), True)
        self.assertEqual(is_prime(8), False)
        self.assertEqual(is_prime(9), False)
        self.assertEqual(is_prime(10), False)
        self.assertEqual(is_prime(11), True)
        # medium
        self.assertEqual(is_prime(289), False)
        self.assertEqual(is_prime(281), True)
        self.assertEqual(is_prime(103), True)
        self.assertEqual(is_prime(111), False)
        # carmichael numbers
        self.assertEqual(is_prime(561), False)
        self.assertEqual(is_prime(41041), False)
        # large
        self.assertEqual(is_prime(100003), True)
        self.assertEqual(is_prime(100007), False)
        # negative
        self.assertEqual(is_prime(0), False)
        self.assertEqual(is_prime(-1), False)
        self.assertEqual(is_prime(-2384), False)
        # exceptions
        self.assertRaisesRegex(ValueError,
            "p must be an integer, got type '<class 'str'>' with value '7'", is_prime, '7')
        self.assertRaisesRegex(ValueError,
            "p must be an integer, got type '<class 'float'>' with value '7.0'", is_prime, 7.0)

    def test_factorize(self):
        # small
        self.assertEqual(factorize(1), [])
        self.assertEqual(factorize(2), [2])
        self.assertEqual(factorize(3), [3])
        self.assertEqual(factorize(4), [2, 2])
        self.assertEqual(factorize(5), [5])
        self.assertEqual(factorize(6), [2, 3])
        self.assertEqual(factorize(7), [7])
        self.assertEqual(factorize(8), [2, 2, 2])
        self.assertEqual(factorize(9), [3, 3])
        self.assertEqual(factorize(10), [2, 5])
        self.assertEqual(factorize(11), [11])
        # medium
        self.assertEqual(factorize(42), [2, 3, 7])
        self.assertEqual(factorize(281), [281])
        self.assertEqual(factorize(289), [17, 17])
        self.assertEqual(factorize(1024), [2] * 10)
        # large
        self.assertEqual(factorize(1789), [1789])
        self.assertEqual(factorize(17893), [29, 617])
        self.assertEqual(factorize(100003), [100003])
        self.assertEqual(factorize(2**25), [2] * 25)
        # exceptions
        self.assertRaisesRegex(ValueError,
            "n must be an integer, got type '<class 'str'>' with value '3'", factorize, '3')
        self.assertRaisesRegex(ValueError,
            "n must be an integer, got type '<class 'float'>' with value '3.0'", factorize, 3.0)
        self.assertRaisesRegex(ValueError, "n must be positive, got '0'", factorize, 0)
        self.assertRaisesRegex(ValueError, "n must be positive, got '-23'", factorize, -23)


if __name__ == '__main__':
    unittest.main()
