ACSL naloge
===========

**This repository is for private use for Gimnazija Vič students and staff only!**

Naloge iz starih tekmovanj so v mapi `stara_tekmovanja`, razporejene po stopnji tekmovanja
in letih. Zahvale Škofijski Gimnaziji za nekatere starejše naloge.

Moje naloge so v mapi `dodatne_naloge`, v kateri si zbrane vse
programerske in teoretične naloge, ki sem jih zbral ali si jih zmislil.
Vse so zbrane v zbriki nalog, ponovljene pa so tudi v kakšnih domačin nalogah.

V mapi `snov` je uradna snov in nekaj malega dodatnega materiala.

V mapi `knjižnica` so zbrane uporabne funkcije, ki se jih potrebuje na tekmovanju.
Zahvala gre ekipi 2016.

Dosežki (v skupini Intermediate 3):

* 2015: 2. mesto, Rok Kos, Bor Brecelj, Matej Marinko
* 2016: 2. mesto, Matej Marinko, Žiga Pačko Koderman, Vid Drobnič
* 2017: 3. mesto, Tim Poštuvan, Jon Mikoš, Jure Bezek
* 2018: 2. mesto, Jon Mikoš, Jakob Pogačnik Souvent, Miha Marinko
* 2019: 3. mesto, Bor Grošelj Simić, Jakob Schrader, Patrik Žnidaršič

Priporočam se za rešitve :)

Jure Slak
