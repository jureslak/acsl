"""Module containing useful functions for evaluating prefix, infix and postfix notations."""

OPERATORS = ['+', '-', '/', '//', '*', '**', '%']

def evaluate_infix(s):
    """Evaluates infix expression as a Python expression from a given string s.
    >>> evaluate_infix('5 + 7')
    12
    """
    if not isinstance(s, str):
        raise ValueError("s must be a string, got type '{}' with value '{}'".format(type(s), s))
    return eval(s)

def evaluate_postfix(s):
    """Evaluates postfix expression from a given string s. All parts of expression must
    be separated with spaces. Unary operators are not supported.
    >>> evaluate_postfix('5 4 +')
    9.0
    >>> evaluate_postfix('3 4 * 2 /')
    6.0
    """
    if not isinstance(s, str):
        raise ValueError("s must be a string, got type '{}' with value '{}'".format(type(s), s))
    stack = []
    for t in s.split():
        try: stack.append(float(t))
        except ValueError:
            if len(stack) >= 2:
                a, b = stack.pop(), stack.pop()
                stack.append(eval(str(b) + t + str(a)))
            else: raise ValueError("invalid postfix expression: '{}'".format(s))
    if len(stack) != 1:
        raise ValueError("invalid postfix expression: '{}'".format(s))
    return stack.pop()

def evaluate_prefix(s):
    """Evaluates prefix expression from a given string s. All parts of expression must
    be separated with spaces. Unary operators are not supported.
    >>> evaluate_prefix('+ 5 4')
    9.0
    >>> evaluate_prefix('/ * 3 4 2')
    6.0
    """
    if not isinstance(s, str):
        raise ValueError("s must be a string, got type '{}' with value '{}'".format(type(s), s))
    stack = []
    for t in reversed(s.split()):
        try: stack.append(float(t))
        except ValueError:
            if len(stack) >= 2:
                a, b = stack.pop(), stack.pop()
                stack.append(eval(str(a) + t + str(b)))
            else: raise ValueError("invalid prefix expression: '{}'".format(s))
    if len(stack) != 1:
        raise ValueError("invalid prefix expression: '{}'".format(s))
    return stack.pop()

def convert_postfix_to_prefix(s):
    """Converts given postfix expression s to prefix. All parts of expression must
    be separated with spaces. Unary operators are not supported.
    >>> convert_postfix_to_prefix('7 5 +')
    '+ 7 5'
    >>> convert_postfix_to_prefix('A B C * +')
    '+ A * B C'
    """
    if not isinstance(s, str):
        raise ValueError("s must be a string, got type '{}' with value '{}'".format(type(s), s))
    stack = []
    for i in s.split():
        if i in OPERATORS:
            if len(stack) >= 2:
                a, b = stack.pop(), stack.pop()
                stack.append(i + ' ' + b + ' ' + a)
            else: raise ValueError("invalid postfix expression: '{}'".format(s))
        else: stack.append(i)
    if len(stack) != 1:
        raise ValueError("invalid postfix expression: '{}'".format(s))
    return stack.pop()

def convert_postfix_to_infix(s):
    """Converts given postfix expression s to infix. All operations are in parentheses.
    All parts of expression must be separated with spaces. Unary operators are not supported.
    >>> convert_postfix_to_infix('7 5 +')
    '(7 + 5)'
    >>> convert_postfix_to_infix('A B C * +')
    '(A + (B * C))'
    """
    if not isinstance(s, str):
        raise ValueError("s must be a string, got type '{}' with value '{}'".format(type(s), s))
    stack = []
    for i in s.split():
        if i in OPERATORS:
            if len(stack) >= 2:
                a, b = stack.pop(), stack.pop()
                stack.append('(' + b + ' ' + i + ' ' + a + ')')
            else: raise ValueError("invalid postfix expression: '{}'".format(s))
        else: stack.append(i)
    if len(stack) != 1:
        raise ValueError("invalid postfix expression: '{}'".format(s))
    return stack.pop()

def convert_prefix_to_postfix(s):
    """Converts given postfix expression s to prefix. All parts of expression must
    be separated with spaces. Unary operators are not supported.
    >>> convert_prefix_to_postfix('+ 6 5')
    '6 5 +'
    >>> convert_prefix_to_postfix('+ A * B C')
    'A B C * +'
    """
    if not isinstance(s, str):
        raise ValueError("s must be a string, got type '{}' with value '{}'".format(type(s), s))
    stack = []
    for i in reversed(s.split()):
        if i in OPERATORS:
            if len(stack) >= 2:
                a, b = stack.pop(), stack.pop()
                stack.append(a + ' ' + b + ' ' + i)
            else: raise ValueError("invalid prefix expression: '{}'".format(s))
        else: stack.append(i)
    if len(stack) != 1:
        raise ValueError("invalid prefix expression: '{}'".format(s))
    return stack.pop()

def convert_prefix_to_infix(s):
    """Converts given prefix expression s to infix. All operations are in parentheses.
    All parts of expression must be separated with spaces. Unary operators are not supported.
    >>> convert_prefix_to_infix('+ 7 5')
    '(7 + 5)'
    >>> convert_prefix_to_infix('+ A * B C')
    '(A + (B * C))'
    """
    if not isinstance(s, str):
        raise ValueError("s must be a string, got type '{}' with value '{}'".format(type(s), s))
    stack = []
    for i in reversed(s.split()):
        if i in OPERATORS:
            if len(stack) >= 2:
                a, b = stack.pop(), stack.pop()
                stack.append('(' + a + ' ' + i + ' ' + b + ')')
            else: raise ValueError("invalid prefix expression: '{}'".format(s))
        else: stack.append(i)
    if len(stack) != 1:
        raise ValueError("invalid prefix expression: '{}'".format(s))
    return stack.pop()

if __name__ == '__main__':
    import doctest
    doctest.testmod()
