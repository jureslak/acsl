n = int(raw_input())
from collections import deque
di = {}
for da in range(n):
    kljuc, vrednost = raw_input().replace(" ", "").split(':')
    vrednost = vrednost.split(',')
    di[kljuc] = vrednost

q = deque()
pomoc = raw_input()
q.append(pomoc)
pomagajo = []
zepomagajo = [pomoc]
while len(q) > 0:
    d = q.popleft()
    for i in di[d]:
        if(i not in zepomagajo):
            pomagajo.append(i)
            zepomagajo.append(i)
            q.append(i)
print ", ".join(sorted(pomagajo))
