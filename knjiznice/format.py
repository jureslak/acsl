"""Useful functions for string formating."""

def bin(number, length=1):
    """
    >>> bin(5, 5)
    '00101'
    >>> bin(10)
    '1010'
    """
    return '{:0>{}b}'.format(number, length)

def round(number, number_of_places):
    """
    >>> round(12.1325241, 3)
    '12.133'
    """
    return '{:.{}f}'.format(number, number_of_places)

def hex(number, upper=True):
    """
    >>> hex(31)
    '1F'
    >>> hex(33, upper=False)
    '21'
    """
    if upper:
        return '{:X}'.format(number)
    else:
        return '{:x}'.format(number)

def oct(number):
    """
    >>> oct(12)
    '14'
    """
    return '{:o}'.format(number)

if __name__ == '__main__':
    import doctest
    doctest.testmod()
