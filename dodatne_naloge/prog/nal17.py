import itertools
r, w, p = map(int, raw_input().split())
rez = 0
for p in itertools.permutations(r*'r' + w*'w' + p*'p'):
    for i in range(len(p)-1):
        if((p[i] == 'r' and p[i+1] == 'w') or (p[i+1] == 'r' and p[i] == 'w')):
            break
    else:
        rez += 1
print rez
