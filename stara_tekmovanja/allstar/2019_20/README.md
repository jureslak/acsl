# ACSL Finals 2020

Zaradi COVID-19 je bilo finalno tekmovanje izvedeno online in individualno.

Programerske naloge so bile v modernem stilu z ocenjevalnim sistemom (Hackerrank).
Junior skupina je imela nalogi 1, 2; Intermediate 2 in 3; Senior 3 in 4.
