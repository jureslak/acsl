def to_troy(weight, unit):
    d = {'GN': 0.0021, 'GM': 0.0321, 'PN': 0.0500, 'TR': 1, 'ON': 0.9115}
    return d[unit] * weight

def to_perc(karats):
    d = {24: 1, 22: 0.917, 18: 0.75, 14: 0.583, 12: 0.5, 8: 0.333, 6: 0.25, 1: 0.04167}
    return d[karats]

price = float(input())

for da in range(5):
    weight, unit, purity, perc = input().split()
    weight, purity, perc = float(weight), float(purity), float(perc)
    weight = to_troy(weight, unit)
    weight *= to_perc(purity)
    p = weight * price
    p *= (1 - perc/100)
    print('{:.2f}'.format(p))
